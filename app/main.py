from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware

from app.api.api_v1.api import api_router
from app.middleware.log import log_middleware
from app.core.config import settings

root_router = APIRouter()
app = FastAPI(title="CU API")

app.middleware("http")(log_middleware)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@root_router.get("/")
def root():
    return {"Welcome": "CU Backend"}


app.include_router(api_router, prefix=settings.API_V1_STR)
app.include_router(root_router)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000, log_level="debug")

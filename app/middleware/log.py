import requests
from fastapi import Request
from app.config.database import SessionLocal
from app.models.log import Log


async def log_middleware(request: Request, call_next):
    ip = request.client.host
    endpoint = request.url.path

    response = requests.get(f"https://ipapi.co/{ip}/json/")
    data = response.json()

    latitud = data.get('latitude')
    longitud = data.get('longitude')
    pais = data.get('country_name')

    db = SessionLocal()
    log = Log(ip=ip, latitud=latitud, longitud=longitud,
              endpoint=endpoint, pais=pais)
    db.add(log)
    db.commit()
    db.close()

    response = await call_next(request)
    return response

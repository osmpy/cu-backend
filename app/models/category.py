from sqlalchemy import Column, Float, Integer, String
from app.config.database import Base


class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, index=True)
    label = Column(String)
    icon = Column(String)

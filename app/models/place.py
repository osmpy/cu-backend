from sqlalchemy import Column, Float, Integer, String, ForeignKey
from app.config.database import Base


class Place(Base):
    __tablename__ = "places"

    id = Column(Integer, primary_key=True, index=True)
    image = Column(String)
    title = Column(String)
    short_description = Column(String)
    long_description = Column(String)
    category_id = Column(Integer, ForeignKey('categories.id'))
    latitude = Column(Float)
    longitude = Column(Float)
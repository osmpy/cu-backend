from sqlalchemy import Column, Float, Integer, String
from app.config.database import Base


class Log(Base):
    __tablename__ = "logs"

    id = Column(Integer, primary_key=True, index=True)
    ip = Column(String)
    latitud = Column(Float)
    longitud = Column(Float)
    endpoint = Column(String)
    pais = Column(String)

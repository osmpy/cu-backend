from fastapi import APIRouter
from typing import Any
from app.config.database import SessionLocal
from app.models.category import Category

router = APIRouter()


@router.get("/")
def categories() -> Any:
    """
    GET categories
    """

    db = SessionLocal()

    result = db.query(Category).all()

    return result

@router.get("/{id}")
def category(id) -> Any:
    """
    GET category by id
    """

    db = SessionLocal()

    result = db.query(Category).get(id)

    return result

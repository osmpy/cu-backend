from fastapi import APIRouter
from typing import Any

router = APIRouter()


@router.get("/routing")
def routing() -> Any:
    """
    Example routing
    """
    result = "Hello routing"

    return result

from fastapi import APIRouter
from typing import Any
from app.config.database import SessionLocal
from app.models.place import Place

router = APIRouter()


@router.get("/")
def places() -> Any:
    """
    GET places
    """

    db = SessionLocal()

    result = db.query(Place).all()

    return result

@router.get("/{id}")
def place(id) -> Any:
    """
    GET place by id
    """

    db = SessionLocal()

    result = db.query(Place).get(id)

    return result

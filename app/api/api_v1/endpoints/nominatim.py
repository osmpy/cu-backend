from fastapi import APIRouter, HTTPException
import requests

router = APIRouter()

@router.get("/search/{q}")
async def get_direction_address_json(q: str, format: str = "jsonv2", limit: int = 1):
    
    """
        Obtiene una ubicación por su descripción textual o por su dirección.
    """
    
    try:
        area_paraguay = "-62.655%2C-19.259%2C-54.141%2C-27.635" # cuadro delimitador de coordenadas más cercanas al área de Paraguay (xmin, ymin, xmax, ymax; x= latitud, y=longitud)
        bounded = "1" # con 'bounded=1' se asegura que los resultados se limiten al cuadro delimitador
        countrycodes = "py"
        response = requests.get(f"https://nominatim.openstreetmap.org/search?q={q}&format={format}&limit={limit}&viewbox={area_paraguay}&bounded={bounded}&countrycodes={countrycodes}")
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as e:
        error_message = e.response.json()["detail"]
        raise HTTPException(status_code=e.response.status_code, detail=error_message)
    except requests.exceptions.JSONDecodeError as je:
        raise HTTPException(status_code=500, detail="El origen ha retornado un formato inválido de JSON.")
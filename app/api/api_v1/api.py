from fastapi import APIRouter

from app.api.api_v1.endpoints import direction, nominatim, places, categories


api_router = APIRouter()
api_router.include_router(
    direction.router, prefix="/directions", tags=["directions"])
api_router.include_router(
    nominatim.router, prefix="/nominatim", tags=["nominatim"])
api_router.include_router(
    places.router, prefix="/places", tags=["places"])
api_router.include_router(
    categories.router, prefix="/categories", tags=["categories"])

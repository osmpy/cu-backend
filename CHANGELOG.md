# Release Notes

## v1.2.0

* [feat] Add nominatim endpoint
* [build] Add env variables
* [docs] Update README.md file
* [fix] Config database
* [build] Removed SQL database as it was automated with Alembic
* [build] Add env variables in docker file

## v1.1.0

* [feat] The log is added in the requests
* [build] Add Database

## v1.0.0

* [feat] Add core
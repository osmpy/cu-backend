# cu-backend

## Tecnologies

* Python 3.11
* Fastapi 0.96.0
* Uvicorn 0.22.0
* PostgreSQL 15
* pgAdmin 7.6
* Alembic 1.11.3
* Docker 24.0.5
* Docker Compose 1.29.2

## Development

### Project

```bash
git clone https://gitlab.com/osmpy/turismo/cu-backend.git
```

#### env variables

```bash
cp .env.example .env
```

Change variables .env

```
# Database
DB_USER=cu_turismo
DB_PASSWORD=123456
DB_HOST=db
DB_PORT=5432
DB_NAME=cu_turismo

# pgAdmin
PGADMIN_DEFAULT_EMAIL=admin@pgadmin.com
PGADMIN_DEFAULT_PASSWORD=admin
```

### Docker

#### Build

```bash
docker-compose up --build
```

#### Start

```bash
docker-compose up
```

#### Down

```bash
docker-compose down
```

#### Migration

```bash
alembic upgrade head
```

#### Web access

```
http://0.0.0.0:8000
```

#### pgAdmin

##### Web access

```
http://0.0.0.0:5050
```

##### Credentials

* Email: admin@pgadmin.com
* Password: admin

## Documentation

### Web access

```
http://0.0.0.0:8000/docs
```
"""create logs table

Revision ID: 241dede54e2a
Revises: 
Create Date: 2023-08-25 09:03:07.230489

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '241dede54e2a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'logs',
        sa.Column('id', sa.Integer, sa.Identity(), primary_key=True),
        sa.Column('ip', sa.String(255)),
        sa.Column('latitud', sa.Double),
        sa.Column('longitud', sa.Double),
        sa.Column('endpoint', sa.String(255)),
        sa.Column('pais', sa.String(255)),
    )

def downgrade() -> None:
    op.drop_table('logs')


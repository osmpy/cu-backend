"""create categories table

Revision ID: a58027d5c311
Revises: 3ccb8f371820
Create Date: 2023-10-10 13:33:32.054356

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a58027d5c311'
down_revision = '3ccb8f371820'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'categories',
        sa.Column('id', sa.Integer, sa.Identity(), primary_key=True),
        sa.Column('label', sa.String(255)),
        sa.Column('icon', sa.String(255))
    )
    op.add_column('places', sa.Column('category_id', sa.Integer, sa.ForeignKey('categories.id')))

def downgrade() -> None:
    op.drop_column('places', 'category_id')
    op.drop_table('categories')

"""create places table

Revision ID: 3ccb8f371820
Revises: 241dede54e2a
Create Date: 2023-09-28 08:15:19.349116

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3ccb8f371820'
down_revision = '241dede54e2a'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'places',
        sa.Column('id', sa.Integer, sa.Identity(), primary_key=True),
        sa.Column('image', sa.String(255)),
        sa.Column('title', sa.String(255)),
        sa.Column('short_description', sa.String(255)),
        sa.Column('long_description', sa.String(1024)),
    )

def downgrade() -> None:
    op.drop_table('places')

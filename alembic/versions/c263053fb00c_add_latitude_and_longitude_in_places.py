"""Add latitude and longitude in places

Revision ID: c263053fb00c
Revises: a58027d5c311
Create Date: 2023-12-30 12:32:20.602426

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c263053fb00c'
down_revision = 'a58027d5c311'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        'places',
        sa.Column('latitude', sa.Double())
    )
    op.add_column(
        'places',
        sa.Column('longitude', sa.Double())
    )


def downgrade() -> None:
    op.drop_column('places', 'latitude')
    op.drop_column('places', 'longitude')

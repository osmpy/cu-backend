FROM python:3.11-slim-bookworm

RUN apt-get update && apt-get install -y build-essential libpq-dev

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt
COPY . /app

RUN chmod +x /app/run.sh

EXPOSE 8000

CMD /app/run.sh
